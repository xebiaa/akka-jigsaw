name := "techrally"

version := "1.0"

resolvers += "spray repo" at "http://repo.spray.io"

libraryDependencies <<= scalaVersion { scala_version => Seq(
  "com.wandoulabs.akka" %% "spray-websocket" % "0.1.1-RC1",
  "io.spray" %% "spray-json" % "1.2.6"
)}
