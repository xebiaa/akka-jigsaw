package com.xebia.akka.jigsaw

abstract class Event

case class PieceMoved(id: String, x: Int, y: Int) extends Event

case class UserJoined(id: String) extends Event

case class UserLeft(id: String) extends Event

case class EventWrapper(pieceMoved: Option[PieceMoved], userJoined: Option[UserJoined])

object EventWrapper {
  def wrap(event: Event): EventWrapper =
    event match {
      case pm: PieceMoved => EventWrapper(Some(pm), None)
      case uj: UserJoined => EventWrapper(None, Some(uj))
    }

  def unwrap(eventWrapper: EventWrapper): Event =
    eventWrapper.pieceMoved match {
      case Some(event) => event
      case None => eventWrapper.userJoined match {
        case Some(event) => event
      }
    }
}

import spray.json.DefaultJsonProtocol
import spray.json.JsonParser

object MyJsonProtocol extends DefaultJsonProtocol {

  implicit val pieceMovedJson = jsonFormat3(PieceMoved)
  implicit val userJoinedJson = jsonFormat1(UserJoined)
  implicit val userLeftJson = jsonFormat1(UserLeft)

  implicit val eventWrapperJson = jsonFormat2(EventWrapper.apply)

}
