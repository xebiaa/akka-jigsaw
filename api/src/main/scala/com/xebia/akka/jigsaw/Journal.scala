package com.xebia.akka.jigsaw

import scala.collection.mutable.ArrayBuffer

class Journal {

  var eventLog = ArrayBuffer[Event]()

  def log(event: Event) = {
    eventLog :+= event
    println(s"logged event: $event")
  }

}
