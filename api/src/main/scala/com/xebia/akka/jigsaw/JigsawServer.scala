package com.xebia.akka.jigsaw

import akka.actor.{Actor, ActorLogging, ActorRef, ActorRefFactory, ActorSystem, Props}
import akka.io.IO
import akka.util.ByteString
import spray.can.server.UHttp
import spray.can.{Http, websocket}
import spray.can.websocket.FrameCommandFailed
import spray.can.websocket.frame.TextFrame
import spray.http.HttpRequest
import spray.routing.HttpServiceActor

object JigSawServer extends App with MySslConfiguration {

  final case class Push(msg: String)

  object WebSocketServer {
    def props() = Props(classOf[WebSocketServer])
  }
  class WebSocketServer extends Actor with ActorLogging {

    val eventProcessor = context.actorOf(EventProcessor.props(new Journal()))

    def receive = {
      // when a new connection comes in we register a WebSocketConnection actor as the per connection handler
      case Http.Connected(remoteAddress, localAddress) =>
        val serverConnection = sender()
        val worker: ActorRef = context.actorOf(WebSocketWorker.props(serverConnection, eventProcessor))
        serverConnection ! Http.Register(worker)
    }
  }

  object WebSocketWorker {
    def props(serverConnection: ActorRef, eventProcessor: ActorRef) = Props(classOf[WebSocketWorker], serverConnection, eventProcessor)
  }
  class WebSocketWorker(val serverConnection: ActorRef, eventProcessor: ActorRef) extends HttpServiceActor with websocket.WebSocketServerConnection {

    import com.xebia.akka.jigsaw.MyJsonProtocol._
    import spray.json._

    override def receive = handshaking orElse businessLogicNoUpgrade orElse closeLogic

    def businessLogic: Receive = {
      // just bounce frames back for Autobahn testsuite
      case TextFrame(f: ByteString) => {
        println(s"received: ${f.utf8String}")
        eventProcessor ! JsonParser(f.utf8String).convertTo[EventWrapper]
      }
      case (topic: String, payload: EventWrapper) => {
        println(s"received from from topic $topic: $payload")
        send(TextFrame(payload.toJson.compactPrint))
      }
      case x: FrameCommandFailed =>
        log.error("frame command failed", x)

      case x: HttpRequest => // do something
    }

    def businessLogicNoUpgrade: Receive = {
      implicit val refFactory: ActorRefFactory = context
      runRoute {
        getFromResourceDirectory("jigsaw")
      }
    }
  }

  def doMain() {
    implicit val system = ActorSystem()

    val server = system.actorOf(WebSocketServer.props(), "websocket")

    IO(UHttp) ! Http.Bind(server, "0.0.0.0", 8080)

    readLine("Hit ENTER to exit ...\n")
    system.shutdown()
    system.awaitTermination()
  }

  // because otherwise we get an ambiguous implicit if doMain is inlined
  doMain()
}
