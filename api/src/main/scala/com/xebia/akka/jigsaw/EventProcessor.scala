package com.xebia.akka.jigsaw

import akka.actor.{ActorRef, Props, Actor}

object EventProcessor {
  def props(journal: Journal) = Props(classOf[EventProcessor], journal)
}

class EventProcessor(journal: Journal) extends Actor {

  override def receive: Receive = {
    case eventWrapper: EventWrapper =>
      val event = EventWrapper.unwrap(eventWrapper)
      event match {
        case e: UserJoined => onUserJoined(sender())
        case _=>
      }
      journal.log(event)
      publish(eventWrapper)
  }

  private def onUserJoined(worker: ActorRef) {
    println(s"start replaying ${journal.eventLog.size} events to $worker")
    journal.eventLog.foreach(e => worker ! "replay" -> EventWrapper.wrap(e))
    println(s"subsribe $worker")
    subscribe(worker)
  }

  private def publish(eventWrapper: EventWrapper) {
    val topic = "all"
    context.system.eventStream.publish(topic, eventWrapper)
    println(s"published to topic $topic: $eventWrapper")
  }

  private def subscribe(worker: ActorRef) {
    context.system.eventStream.subscribe(worker, classOf[(String, EventWrapper)])
  }
}
